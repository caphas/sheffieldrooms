<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistingTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('existing_tenants', function (Blueprint $table) {
            $table->increments('id_existing_tenant');
            $table->boolean('hates_football');
            $table->boolean('vegetarian');
            $table->boolean('professional');
            $table->boolean('student');
            $table->boolean('professional_student');
            $table->boolean('cat');
            $table->boolean('dog');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('existing_tenants');
    }
}
