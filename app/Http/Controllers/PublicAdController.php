<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PublicAdController extends Controller
{
    //Get All Ads
    public function getAllAds()
    {
        //Ads 

        $ads=DB::table('ads')
                ->join('addresses', 'ads.id_address', '=', 'addresses.id_address')
                ->join('tenancy_details', 'ads.id_tenancy_detail', '=', 'tenancy_details.id_tenancy_detail')
                ->select('ads.*', 'addresses.*', 'tenancy_details.*')
                ->get();
        //Address
        // $postcode=DB::table('addresses')
        //     ->where('id_address',$ad->id_address)
        //     ->get()->first()->postcode;
        // $house_number=DB::table('addresses')
        //     ->where('id_address',$ad->id_address)
        //     ->get()->first()->house_number;
        // $address_line_1=DB::table('addresses')
        //     ->where('id_address',$ad->id_address)
        //     ->get()->first()->address_line_1;
        // $town=DB::table('addresses')
        //     ->where('id_address',$ad->id_address)
        //     ->get()->first()->town;


        return $ads;
    }
}
