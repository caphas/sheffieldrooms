<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

// Route::get('/dashboard', function () {
//     return view('private.dashboard');
// })->name('dashboard');

Route::get('/dashboard/mylistings', function () {
    return view('private.mylistings');
})->name('mylistings');

Route::get('/dashboard/placeanad', function () {
    return view('private.placeanad');
})->name('placeanad');

Route::get('/dashboard/messages', function () {
    return view('private.messages');
})->name('messages');

Route::resource('private.ad', 'AdController');
 
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');


//Management of Adds 
Route::get('/API/Rest/V1/getAllAds','PublicAdController@getAllAds');
