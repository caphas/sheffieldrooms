<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExistingTenant extends Model
{
    //
    protected $fillable = [
        'id_existing_tenant', 'hates_football', 'vegetarian','professional', 'student' , 'professional_student','cat', 'dog' 
    ];
}
