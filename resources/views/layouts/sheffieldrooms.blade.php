<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta Tag -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="sheffieldrooms">
		<meta name="keywords" content="real estate, property, property search, agent, apartments, booking, business, idx, housing, real estate agency, rental">
		<meta name="author" content="unicoder">
         <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Sheffieldrooms</title>
		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico">
		
		<!-- Bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/font-awesome.css">
		<link rel="stylesheet" href="fonts/flaticon.css">
		<link rel="stylesheet" href="css/colors/blue.css">
		<link rel="stylesheet" href="css/jslider.css">
		<link rel="stylesheet" href="css/responsive.css">
		<link rel="stylesheet" href="css/loader.css">
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>
	<body class="pagewrap property-list-page">
		
		<!-- Page Loader -->
		<div class="loading-page">
			<div class="sk-circle">
			  <div class="sk-circle1 sk-child"></div>
			  <div class="sk-circle2 sk-child"></div>
			  <div class="sk-circle3 sk-child"></div>
			  <div class="sk-circle4 sk-child"></div>
			  <div class="sk-circle5 sk-child"></div>
			  <div class="sk-circle6 sk-child"></div>
			  <div class="sk-circle7 sk-child"></div>
			  <div class="sk-circle8 sk-child"></div>
			  <div class="sk-circle9 sk-child"></div>
			  <div class="sk-circle10 sk-child"></div>
			  <div class="sk-circle11 sk-child"></div>
			  <div class="sk-circle12 sk-child"></div>
			</div>
		</div>
		<!-- End Loader -->
		
		<!-- Color Settings -->
		<!-- <div class="blue" data-path="css/colors/blue.css" data-image="img/logo1_blue.png" data-target="img/logo2_blue.png"></div> -->
		<!-- <div class="color-panel">
			<div class="on-panel"><img src="img/icons/settings.png" alt=""></div>
			<div class="panel-box">
				<span class="panel-title">Change Colors</span>
				<ul class="color-box">
					<li class="green" data-path="css/colors/green.css" data-image="img/logo1.png" data-target="img/logo2.png"></li>
					<li class="blue" data-path="css/colors/blue.css" data-image="img/logo1_blue.png" data-target="img/logo2_blue.png"></li>
					<li class="red" data-path="css/colors/red.css" data-image="img/logo1_red.png" data-target="img/logo2_red.png"></li>
					<li class="purple" data-path="css/colors/purple.css" data-image="img/logo1_purple.png" data-target="img/logo2_purple.png"></li>
					<li class="yellow" data-path="css/colors/yellow.css" data-image="img/logo1_yellow.png" data-target="img/logo2_yellow.png"></li>
					<li class="orange" data-path="css/colors/orange.css" data-image="img/logo1_orange.png" data-target="img/logo2_orange.png"></li>
					<li class="magento" data-path="css/colors/magento.css" data-image="img/logo1_magento.png" data-target="img/logo2_magento.png"></li>
					<li class="turquoise" data-path="css/colors/turquoise.css" data-image="img/logo1_turquoise.png" data-target="img/logo2_turquoise.png"></li>
					<li class="lemon" data-path="css/colors/lemon.css" data-image="img/logo1_lemon.png" data-target="img/logo2_lemon.png"></li>
				</ul>
			</div>
		</div> -->
		<!-- End Color Settings -->
		
		<header id="header">
			<!-- Top Header Start -->
			<div id="top_header">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-3">
							<div class="top_contact">
								<a class="navbar-brand" href="{{route('index')}}"><img class="nav-logo" src="img/logo.png" alt=""></a>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="top_contact">
								<a class="navbar-brand" href="{{route('index')}}"><img class="nav-logo" src="img/slogo.png" alt=""></a>
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="top_right">
								<ul>
									<!-- <li>
										<div class="lan-drop"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Eng <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#">sp</a></li>
												<li><a href="#">ch</a></li>
												<li><a href="#">ud</a></li>
											</ul>
										</div>
									</li> -->
									<li><a href="{{route('login')}}"><button  class="btn btn-primary btn-sm" >Place an AD</button></a></li>
									<!-- <li><a href="{{route('register')}}"><button class="btn btn-default btn-sm" >Register</button></a></li>
									<li><a href="{{route('login')}}" ><button class="btn btn-default btn-sm" >Log In</button></a></li> -->
									<!-- Authentication Links -->
									@guest
										@if (Route::has('register'))
											<li class="nav-item">
												<a href="{{ route('register') }}"><button class="btn btn-default btn-sm" >Register</button></a>
											</li>
										@endif
										<li  >
											<a   href="{{ route('login') }}"><button class="btn btn-default btn-sm" >Log In</button></a>
										</li>
									@else
										<li  >
											<div class="btn-group lan-drop" role="group">
												<button type="button" class="btn btn-default btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-left-color: rgb(209, 209, 209);">
													{{ Auth::user()->name }} 
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
													<li>
														<a   href="{{ route('login') }}">My profile</a>
													</li>
													<li>
														<a class="dropdown-item" href="{{ route('logout') }}"
														onclick="event.preventDefault();
																		document.getElementById('logout-form').submit();">
															{{ __('Logout') }}
														</a>

														<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
															@csrf
														</form>
													</li>
												</ul>
											</div>
										</li>
									@endguest
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Top Header End --> 
			
		</header>
            <!-- Banner Section Start -->
		<section>
			
        </section>

        @yield('content')
 
		
		<!-- Bottom Footer Start -->
		<div id="bottom_footer">
			<div class="reserve_text"> <span>Copyright &copy; 2017 Uniland All Right Reserve</span> </div>
		</div>
		<!-- Bottom Footer End -->
		
		<!-- Scroll to top -->
		<div class="scroll-to-top">
			<a href="#" class="scroll-btn" data-target=".pagewrap"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
		</div>
		 
		<!-- End Scroll To top -->
        <!-- All Javascript Plugin File here -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-select.js"></script>
		<script src="js/YouTubePopUp.jquery.js"></script>
		<script src="js/jquery.fancybox.pack.js"></script>
		<script src="js/jquery.fancybox-media.js"></script>
		<script src="js/owl.js"></script>
		<script src="js/mixitup.js"></script>
		<script src="js/wow.js"></script>
		<script src="js/jshashtable-2.1_src.js"></script> 
		<script src="js/jquery.numberformatter-1.2.3.js"></script> 
		<script src="js/tmpl.js"></script>
		<script src="js/jquery.dependClass-0.1.js"></script> 
		<script src="js/draggable-0.1.js"></script> 
		<script src="js/jquery.slider.js"></script>
		<script src="js/jquery.barrating.js"></script>
		<script src="js/custom.js"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-106986305-1', 'auto');
			ga('send', 'pageview');
			
			
		</script>
		
	</body>
</html>	