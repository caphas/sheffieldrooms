@extends('layouts.private')

@section('content')

<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> My Listings </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        My Listings
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Ref #</th>
                                        <th>Address</th>
                                        <th>Created</th>
                                        <th>Publish</th>
                                        <th>Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($ads as $ad)
                                    <tr class="odd gradeX">
                                        <td>{{$ad->id_ad}}</td>
                                        <?php 
                                            $postcode=DB::table('addresses')->where('id_address',$ad->id_address)->get()->first()->postcode;
                                            $house_number=DB::table('addresses')->where('id_address',$ad->id_address)->get()->first()->house_number;
                                            $address_line_1=DB::table('addresses')->where('id_address',$ad->id_address)->get()->first()->address_line_1;
                                            $town=DB::table('addresses')->where('id_address',$ad->id_address)->get()->first()->town;
                                         ?>
                                        <td>House N°: {{$house_number}},{{$address_line_1}},{{$postcode}},{{$town}}</td>
                                        <td>{{$ad->created_at}}</td>
                                        <td class="center"><input type="button" value="Duplicate" class="btn btn-ms btn-primary"></td>
                                        <td>
                                            <form
                                            action="{{route('private.ad.destroy',['ad'=>$ad->id_ad,'account'=>$account])}}"
                                            method="POST"
                                            onsubmit="return confirm('Etes vous sur ?');"
                                            >
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}
                                                <input type="submit" value="Remove" class="btn btn-ms btn-danger">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
                <!-- /.col-lg-12 -->
        </div>
</div>
<!-- Content -->


@endsection