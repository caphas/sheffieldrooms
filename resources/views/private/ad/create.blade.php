@extends('layouts.private')

@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <form  class="form-horizontal" action="{{route('private.ad.store',['account'=>$account])}}" method="POST" enctype="multipart/form-data" >
                      {{csrf_field()}}
                <div class="form-group">
                        <h1 style="color:#428bca;">1.Who are you ? *</h1>
                </div>
                <div class="form-group {{ $errors->has('advertiser_type') ? 'has-error':''}}">
                    <select class="selectpicker" name="advertiser_type">
                        <option value="" selected>Please select -></option>
                        <option value="Live in landlord">Live in landlord (I own the property and live there)</option>
                        <option value="Live out landlord ">Live out landlord (I own the property but don't live there)</option>
                        <option value="Current tenant/flatmate">Current tenant/flatmate (I am living in the property)</option>
                        <option value="Agent">Agent (I am advertising on a landlord's behalf)</option>
                        <option value="Former">Former flatmate (I am moving out and need someone to replace me)</option>
                    </select>
                    @if ($errors->has('advertiser_type'))
                            <div class="alert alert-danger" role="alert">{{ $errors->first('advertiser_type') }}</div>
                    @endif
                    <!-- <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                        <label class="form-check-label" for="inlineRadio1">Live in landlord <span style="color:rgb(163, 163, 163);">(I own the property and live there)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Live out landlord <span style="color:rgb(163, 163, 163);">(I own the property but don't live there)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Current tenant/flatmate <span style="color:rgb(163, 163, 163);">(I am living in the property)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Agent <span style="color:rgb(163, 163, 163);">(I am advertising on a landlord's behalf)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Former flatmate <span style="color:rgb(163, 163, 163);">(I am moving out and need someone to replace me)</span></label>
                    </div> -->
                </div>
                <hr/>
                 
                <div class="form-group">
                    <p> Fee applies ? (e.g. admin fees, tenant referencing, fees for drawing up tenancy agreements)</p>
                    <input type="checkbox" checked data-off-active-cls="btn-warning" data-on-active-cls="btn-primary" name="fee_applies">
                    <p>Fees apply ?<br/> (e.g. admin fees, tenant referencing, fees for drawing up tenancy agreements)<br/>
                        Tick for Yes ( Landlords and Agents must disclose any non-optional fees when advertising rentals,
                        according to CAP rules. <br/>You should itemise what the fees cover, and how much they will cost each tenant in your description lower down. )</p>
                </div>
                      
                <hr/>
                <h1 style="color:#428bca;">2. What are you renting ?</h1>
                <div class="form-group row">
                    <div class="col-lg-8">
                        <input id="id_rent_type" type="checkbox" data-group-cls="btn-group-justified" name="rent_type">
                    </div>
                    <hr/>
                    <div class="col-lg-4">
                        <p>Advertise one or more rooms in a property e.g. house, flat.</p>    
                    </div>
                    <div class="col-lg-4">
                        <p>Advertise a whole property. This include houses, flats, including one-bedroom flats, studios and bedsits.
                        <br/>There are no existing flatmates and there will be a single contract.</p>
                    </div>
                    <script>
                        $(document).ready(function() {
                            $('#id_rent_type').checkboxpicker({
                            html: true,
                                offLabel: 'Room(s)',
                                onLabel: 'Entire Property/Studio'
                            });
                        });
                    </script>
                </div>
                <hr/>
                <div class="form-group">
                    <h1 style="color:#428bca;">3. The address of the property to rent</h1>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter postcode here">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">FIND ADDRESS</button>
                            </span>
                        </div><!-- /input-group -->
                    </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="form-group">
                        <select class="selectpicker">
                            <option value="" selected>Please select -></option>
                            <option value="1">We Couldn't find your address</option>
                            <!-- <option value="2">Two</option>
                            <option value="3">Three</option> -->
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h2 style="color:#428bca;">Or manually</h2>
                </div>            
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('postcode') ? 'has-error':''}}">
                        <label><strong>Postcode: *</strong></label>
                        <input class="form-control" name="postcode" placeholder="Enter postcode here">
                        @if ($errors->has('postcode'))
                            <div class="alert alert-danger" role="alert">{{ $errors->first('postcode') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('house_number') ? 'has-error':''}}">
                        <label><strong>Flat or House Number (kept private): *</strong></label>
                        <input class="form-control" name="house_number" placeholder="Enter house number here">
                        @if ($errors->has('house_number'))
                            <div class="alert alert-danger" role="alert">{{ $errors->first('house_number') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Address line 1 (optional):</strong></label>
                        <input class="form-control" name="address_line_1" placeholder="eg. 10 Downing St or Westminster">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Address line 2 (optional):</strong></label>
                        <input class="form-control" name="address_line_2" placeholder="eg. 10 Downing St or Westminster">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Town *:</strong></label>
                        <input class="form-control" name="town" placeholder="Town">
                    </div>
                </div>
                <hr/>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">4. Type of property</h1>
                </div> 
                <div class="col-lg-6">
                    <div class="form-group">
                        <select class="selectpicker" name="proprety_type">
                            <optgroup label="Room in a Shared Property">
                                <option>Room in a Shared House</option>
                                <option>Room in a Shared Flat</option>
                            </optgroup>
                            <optgroup label="Single Occupancy">
                                <option>Studio</option>
                                <option>Bedsit</option>
                            </optgroup>
                            <optgroup label="Entire House">
                                <option>Detached House</option>
                                <option>Semi-Detached House</option>
                                <option>Terraced House</option>
                                <option>End Terrance</option>
                                <option>Bungalow</option>
                                <option>Cottage</option>
                            </optgroup>
                            <optgroup label=" Entire Flat">
                                <option>Flat</option>
                                <option>Penthouse</option>
                                <option>Maisonette</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">5. Room type</h1>
                </div> 
                <div class="form-group row">
                    <div class="col-lg-4 {{ $errors->has('date_available') ? 'has-error':''}}">
                        <label><strong>Date available * :</strong></label>
                        <input class="form-control" name="date_available" placeholder="eg. 2018/10/10">
                        @if ($errors->has('date_available'))
                            <div class="alert alert-danger" role="alert">{{ $errors->first('date_available') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6 {{ $errors->has('property_type') ? 'has-error':''}}">
                        <label  class="col-lg-4"><strong>Property type :</strong></label>
                        <select class="selectpicker col-lg-8" name="property_type">
                            <option value="" selected>Please select -></option>
                        </select>
                    </div>
                    <div class="col-lg-6 {{ $errors->has('room_type') ? 'has-error':''}}">
                        <label class="col-lg-4"><strong>Room type :</strong></label>
                        <select class="selectpicker col-lg-8" name="room_type">
                            <option value="" selected>Please select -></option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">      
                        <label><strong>Available to couples :</strong></label>      
                        <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="available_couple">
                    </div>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">6. Add photos/video</h1>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">File:</label>
                    <div class="col-lg-10">
                        <input id="input-b1"  name="photos[]"  multiple type="file" class="file" data-browse-on-zone-click="true"  data-allowed-file-extensions='["png", "jpg","jpeg"]'>
                    </div>
                </div> 
                <div class="form-group row">
                    <label><strong>Optional: Add YouTube share URL  :</strong></label>
                    <input class="form-control" name="youtube_url" placeholder="https://www.youtube.com/">
                </div>
                <div class="col-lg-12">
                    <h2 style="color:#428bca;">The Map: </h2>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">7. Provide information about the tenancy</h1>
                </div>
                <h3> Tenancy Details</h3>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>Monthly Rent For Entire Studio:</label>
                        <input class="form-control" placeholder="eg. 400£" name="monthlyRent">
                    </div>
                    <div class="col-lg-4">
                        <label>Minimum Tenancy Length</label>
                        <select nam="tenancyLength" class="selectpicker">
                            <option value="1">1 Month</option>
                            <option value="3">3 Months</option>
                            <option value="6">6 Months</option>
                            <option value="12">1 Year</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Earliest Move In Date:</label>
                        <select class="selectpicker" >
                            <option selected>Please select -></option>
                        </select>
                    </div>
                    <div class="col-lg-4">      
                        <label>Weekly Rent For Entire Studio:</label>      
                        <input class="form-control" placeholder="eg.  100£">
                    </div>
                    <div class="col-lg-4">      
                        <label>Maximum Tenancy length (optional):</label>      
                        <select nam="maxTenancy" class="selectpicker">
                            <option value="1">1 Month</option>
                            <option value="3">3 Months</option>
                            <option value="6">6 Months</option>
                            <option value="12">1 Year</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">      
                        <label>Deposit Amount:</label>      
                        <input name="depositAmount" class="form-control" placeholder="eg.  300£">
                    </div>
                    <div class="col-lg-4">      
                        <label>Maximum Number of Tenants:</label>      
                        <input name="maxNumberTenant" class="form-control" placeholder="eg. 2">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Housing beenfit considered ?:</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="housingBeenfit">
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Reference required ?:</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="referenceRequired">
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Bills:</strong></label>
                    <label class="col-lg-12">Tick appropriate box.</label>
                    <label class="col-lg-12"><strong> Bills included ?</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="billsIncluded">
                    <!-- <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Some</button>
                    </div> -->
                </div>
                <div class="form-group">
                        <label><strong>Which are included :</strong></label>
                        <select name="includeds[]" class="selectpicker" multiple title="Choose one of the following...">
                            <option value="1">Council Tax</option>
                            <option value="2">Water</option>
                            <option value="3">Electricity</option>
                            <option value="4">Gas</option>
                            <option value="5">Wi-Fi /BroadBand</option>
                            <option value="6">TV</option>
                            <option value="7">License</option>
                            <option value="8">Cleaning</option>
                        </select>
                </div>
                <hr/>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">8. Bedroom</h1>
                </div>
                <div class="form-group">
                    <label><strong>Which are included :</strong></label>
                    <select name="bedRoomSelection[]" class="selectpicker" multiple title="Choose one of the following...">
                        <option value="1">The room has a Double Bed.</option>
                        <option value="2">It is a double room (this means it's a big room, not that it has a double bed in it)</option>
                        <option value="3">The room could go to a couple (you should mention rent adjustments in you description)</option>
                        <option value="4">The Room has its own bathroom</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">9. Features about your property</h1>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">10. The existing tenants</h1>
                </div>
                <div class="form-group">
                    <select name="existingTenants[]" class="selectpicker" multiple >
                        <option value="1">The existing household hates football</option>
                        <option value="2">The existing household is vegetarian</option>
                        <option value="3">The existing household are professionals</option>
                        <option value="4">The existing household are students</option>
                        <option value="5">The existing household has both professional and students</option>
                        <option value="6">There is a cat</option>
                        <option value="7">There is a dog</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">Preference for new flatmate...</h1>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Gender:</strong></label>
                    <select nam="gender" class="selectpicker">
                        <option value="No preference">No preference</option>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>
                        <option value="Mix">Mix</option>
                    </select>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Occupation:</strong></label>
                    <select nam="occupation" class="selectpicker">
                        <option value="No preference">No preference</option>
                        <option value="Professional">Professional</option>
                        <option value="Student">Student</option>
                    </select>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Pets:</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="pets">
                    <!-- <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div> -->
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Age of tenant:</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="tenantAge">
                    <!-- <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div> -->
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Couples welcome?:</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="couples">
                    <!-- <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div> -->
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Vegetarian preferred?:</strong></label>
                    <input type="checkbox" checked data-off-active-cls="btn-danger" data-on-active-cls="btn-success" name="vegetarian">
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">How to contact you...</h1>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label class="col-lg-3">
                            <input type="checkbox" value="">Email: 
                        </label>
                        <input type="text" name="email_contact" placeholder="example@mail.com">
                    </div>
                    <div class="checkbox">
                        <label class="col-lg-3">
                            <input type="checkbox" value="">Phone:
                        </label>
                        <input type="text" name="phone_contact" placeholder="Phone number">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8">
                        <label>By selecting Post My Ad you agree you've read and accepted our Terms of Use an Posting Rules. Please see our
                        Privacy Notice for information regarding the processing of your data</label>
                    </div>
                    <div class="col-lg-4">
                        <button type="submit" class="btn btn-danger">Post My ad</button>
                    </div>
                </div>
                <!--<div class="form-group row">
                    <label class="col-lg-12"><strong>Single Occupancy:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Studio</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Bedsit</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Entire House:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Semi-Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Terraced House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">End Terrance</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Bungalow</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Cottage</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Entire Flat:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Semi-Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Terraced House</button>
                    </div>
                </div> -->
   

            </form>
        </div>
    </div>
</div>
<!-- Content -->

@endsection