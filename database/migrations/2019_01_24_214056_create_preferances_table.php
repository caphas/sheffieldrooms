<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferances', function (Blueprint $table) {
            $table->increments('id_preferance');
            $table->string('gender',10)->nullable();
            $table->string('occupation',36)->nullable();
            $table->boolean('pets');
            $table->boolean('age_tenant');
            $table->boolean('couple_welcome');
            $table->boolean('vegetarian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferances');
    }
}
