<?php

namespace App\Http\Controllers;

use App\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account)
    {
        //
        $ads=DB::table('ads')
            ->where('id', '=', $account)
            ->get();
        //dd($ads);
        return view('private.ad.index',compact('account','ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($account)
    {
        //
        return view('private.ad.create',compact('account'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$account)
    {
        //
    
        $this->validate($request,[
            'advertiser_type'=>'required',
            'postcode'=>'required',
            'house_number'=>'required',
            'town'=>'required'
        ]);
       
        //Save Included
        $idIcluded=$this->saveIncludeds(
            $request->includeds
        );

        //Save tenancy
        $idTenancy=$this->saveTenancy(
            $idIcluded,$request->tenancyLength
            ,$request->monthlyRent,$request->maxTenancy
            ,$request->depositAmount,$request->maxNumberTenant
            ,$request->housingBeenfit,$request->referenceRequired
            ,$request->billsIncluded
        );
        //Save Preference
        $idPreference=$this->savePreference(
            $request->gender,$request->occupation
            ,$request->pets,$request->tenantAge,$request->couples
            ,$request->vegetarian
        );
        //Save Address
        $idAddress=$this->saveAddress(
            $request->postcode,$request->house_number,
            $request->address_line_1,$request->address_line_2
            ,$request->town
        );
        
        //Save existing_tenants
        $idExistingTenants=$this->saveExistingTenants(
            $request->existingTenants
        );
        
        //Save Room types
        $idRoomTypes=$this->saveRoomType(
            $request->date_available,
            $request->property_type,
            $request->room_type,
            $request->available_couple
        );

        //Save Bedrooms
        $idBedrooms=$this->saveBedroom(
            $request->bedRoomSelection
        );
        
        //Save ads
        $idAds=$this->saveAds(
            $request->advertiser_type,
            $request->fee_applies,
            $request->rent_type,
            $request->proprety_type,
            $request->youtube_url,
            $request->email_contact,
            $request->phone_contact,
            $idAddress,
            $idRoomTypes,
            $idTenancy,
            $idExistingTenants,
            $idPreference,
            $idBedrooms,
            $account
        );
        //Save photo
        $this->savePhotos($idAds,$request->photos);

        $ads=DB::table('ads')
                ->where('id', '=', $account)
                ->get();
        return view('private.ad.index',compact('account','ads'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_ad,$account)
    {
        //
    }
    //Save Includs
    private function saveIncludeds($selectionIncludeds)
    {
        $council=false;
        $water=false;
        $electrecity=false;
        $gas=false;
        $wifi=false;
        $tv=false;
        $licence=false;
        $cleaning=false;
        if(!(is_null($selectionIncludeds)))
        {
            $nSelection = count($selectionIncludeds);
            for($i=0; $i < $nSelection; $i++)
            {
                $numberVal = $selectionIncludeds[$i];
                if ($numberVal == "1"){
                    $council=true;
                }
                if ($numberVal == "2"){
                    $water=true;
                } 
                if ($numberVal == "3"){
                    $electrecity=true;
                }
                if ($numberVal == "4"){
                    $gas=true;
                }
                if ($numberVal == "5"){
                    $wifi=true;
                }
                if ($numberVal == "6"){
                    $tv=true;
                }
                if ($numberVal == "7"){
                    $licence=true;
                }
                if ($numberVal == "8"){
                    $cleaning=true;
                }
            }
        }
        $id_included_last= DB::table('includeds')-> insertGetId(array(
            'council_tax'=>$council,
            'water'=>$water,
            'electricity'=>$electrecity, 
            'gas'=>$gas,
            'wifi'=> $wifi,
            'tv'=>$tv,
            'license'=>$licence,
            'cleaning'=>$cleaning
        ));

        
        return $id_included_last;
    }

    //Save Tenancy
    private function saveTenancy($idIcluded,$tenancyLength,$monthlyRent,$maxTenancy
                                ,$depositAmount,$maxNumberTenant
                                ,$housingBeenfit,$referenceRequired
                                ,$billsIncluded)
    {
        if($housingBeenfit=="on")
            $housingBeenfit=true;
        else
            $housingBeenfit=false;
        if($referenceRequired=="on")
            $referenceRequired=true;
        else
            $referenceRequired=false;
        if($billsIncluded=="on")
            $billsIncluded=true;
        else
            $billsIncluded=false;

        $id_tenancy_last= DB::table('tenancy_details')-> insertGetId(array(
            'monthly_rent'=>$monthlyRent,
            'min_tenancy_length'=>$tenancyLength,
            'earlest_move_date'=>null,  //A  refaire
            'weekly_rent'=>null,//A  refaire
            'max_tenancy_length'=> $maxTenancy,
            'deposit_amount'=>$depositAmount,
            'max_tenants'=>$maxNumberTenant,
            'housing_beenfit'=>$housingBeenfit,
            'reference_required'=>$referenceRequired,
            'bills_included'=>$billsIncluded,
            'id_included'=>$idIcluded,

        ));
        return $id_tenancy_last;
    }
    private function savePreference($gender,$occupation
                    ,$pets,$tenantAge,$couples,$vegetarian)
    {
        if($pets=="on")
            $pets=true;
        else
            $pets=false;
        if($tenantAge=="on")
            $tenantAge=true;
        else
            $tenantAge=false;
        if($couples=="on")
            $couples=true;
        else
            $couples=false;
        if($vegetarian=="on")
            $vegetarian=true;
        else
            $vegetarian=false;
        $id_preference_last= DB::table('preferances')-> insertGetId(array(
            'gender'=>$gender,
            'occupation'=>$occupation,
            'pets'=>$pets,   
            'age_tenant'=>$tenantAge, 
            'couple_welcome'=> $couples,
            'vegetarian'=>$vegetarian

        ));
        return $id_preference_last;
    }

    private function saveAddress($postcode,$house_number,
            $address_line_1,$address_line_2
            ,$town)
    {
        $id_address_last=DB::table('addresses')-> insertGetId(array(
            'postcode'=>$postcode,
            'house_number'=>$house_number,
            'address_line_1'=>$address_line_1, 
            'address_line_2'=>$address_line_2,
            'town'=> $town
        ));
        return $id_address_last;
    }

    private function saveExistingTenants(
        $existingTenants
    )
    {
        $hates_football=false;
        $vegetarian=false;
        $professional=false;
        $student=false;
        $professional_student=false;
        $cat=false;
        $dog=false;
        if(!(is_null($existingTenants)))
        {
            $nSelection = count($existingTenants);
            for($i=0; $i < $nSelection; $i++)
            {
                $numberVal = $existingTenants[$i];
                if ($numberVal == "1"){
                    $hates_football=true;
                }
                if ($numberVal == "2"){
                    $vegetarian=true;
                } 
                if ($numberVal == "3"){
                    $professional=true;
                }
                if ($numberVal == "4"){
                    $student=true;
                }
                if ($numberVal == "5"){
                    $professional_student=true;
                }
                if ($numberVal == "6"){
                    $cat=true;
                }
                if ($numberVal == "7"){
                    $dog=true;
                }
            }
        }
        $id_existing_tenants=DB::table('existing_tenants')-> insertGetId(array(
            'hates_football'=>$hates_football,
            'vegetarian'=>$vegetarian,
            'professional'=>$professional, 
            'student'=>$student,
            'professional_student'=> $professional_student,
            'cat'=>$cat,
            'dog'=>$dog,
        ));

        return $id_existing_tenants;

    }
    
    private function saveRoomType(
        $date_available,
        $property_type,
        $room_type,
        $available_couple
    )
    {
        if($available_couple=="on")
            $available_couple=true;
        else
            $available_couple=false;
        $idRoomTypeLast=DB::table('room_types')-> insertGetId(array(
            'date_available'=>$date_available,
            'property_type'=>$property_type,
            'room_type'=>$room_type, 
            'available_couple'=>$available_couple
        ));
        return $idRoomTypeLast;
    }

    private function saveBedroom(
        $bedRoomSelection
    )
    {
        $double_bed=false;
        $double_room=false;
        $couple=false;
        $bathroom=false;
        if(!(is_null($bedRoomSelection)))
        {
            $nSelection = count($bedRoomSelection);
            for($i=0; $i < $nSelection; $i++)
            {
                $numberVal = $bedRoomSelection[$i];
                if ($numberVal == "1"){
                    $double_bed=true;
                }
                if ($numberVal == "2"){
                    $double_room=true;
                } 
                if ($numberVal == "3"){
                    $couple=true;
                }
                if ($numberVal == "4"){
                    $bathroom=true;
                }
            }
        }
        $idBedroomLast=DB::table('bedrooms')-> insertGetId(array(
            'double_bed'=>$double_bed,
            'double_room'=>$double_room,
            'couple'=>$couple, 
            'bathroom'=>$bathroom
        ));

        return $idBedroomLast;
    }
    private function saveAds(
        $advertiser_type,
        $fee_applies,
        $rent_type,
        $proprety_type,
        $youtube_url,
        $email_contact,
        $phone_contact,
        $idAddress,
        $idRoomTypes,
        $idTenancy,
        $idExistingTenants,
        $idPreference,
        $idBedrooms,
        $account
    )
    {
        if($fee_applies=="on")
            $fee_applies=true;
        else
            $fee_applies=false;
        $idAdLast=DB::table('ads')-> insertGetId(array(
            'advertiser_type'=>$advertiser_type,
            'fee_applies'=>$fee_applies,
            'rent_type'=>$rent_type, 
            'proprety_type'=>$proprety_type,
            'youtube_url'=>$youtube_url,
            'email_contact'=>$email_contact,
            'phone_contact'=>$phone_contact, 
            'id_address'=>$idAddress,

            'id_room_type'=>$idRoomTypes,
            'id_tenancy_detail'=>$idTenancy, 
            'id_existing_tenant'=>$idExistingTenants,

            'id_preferance'=>$idPreference,
            'id_bedroom'=>$idBedrooms, 
            'id'=>$account,

        ));

        return $idAdLast;
    }

    //Save Photo
    private function savePhotos($idAd,$fileFromRequest)
    {
        $path="pictures/";
        $files=$fileFromRequest;
        if(!(is_null($files)))
        {
            $nFiles = count($files);
            for($i=0; $i < $nFiles; $i++)
            {
                $file=$files[$i];
                $name=time().'_'.$file->getClientOriginalName();
                $file->move(base_path().'/public/'.$path,$name);
                
                $idPhotoLast=DB::table('photos')-> insertGetId(array(
                    'id_ad'=>$idAd,
                    'emplacement'=>base_path().'/public/'.$path.$name,
                ));
            }
        }
    }
}
