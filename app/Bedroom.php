<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bedroom extends Model
{
    //
    protected $fillable = [
        'id_bedroom', 'double_bed', 'double_room','couple', 'bathroom' 
    ];
     
}
