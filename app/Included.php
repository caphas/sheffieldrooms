<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Included extends Model
{
    //
    protected $fillable = [
        'id_included', 'council_tax', 'water','electricity', 'gas' , 'wifi','tv', 'license','cleaning'
         
    ];
}
