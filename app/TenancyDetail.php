<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenancyDetail extends Model
{
    //
    protected $fillable = [
        'id_tenancy_detail', 'monthly_rent', 'min_tenancy_length' , 'earlest_move_date', 'weekly_rent',
        'max_tenancy_length', 'deposit_amount', 'max_tenants' , 'housing_beenfit', 'reference_required',
        'bills_included','id_included'
    ];
}
