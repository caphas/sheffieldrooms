<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenancyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenancy_details', function (Blueprint $table) {
            $table->increments('id_tenancy_detail');
            $table->integer('monthly_rent')->nullable();
            $table->string('min_tenancy_length',192)->nullable();
            $table->date('earlest_move_date')->nullable();
            $table->integer('weekly_rent')->nullable();
            $table->integer('max_tenancy_length')->nullable();
            $table->integer('deposit_amount')->nullable();
            $table->integer('max_tenants')->nullable();
            $table->boolean('housing_beenfit');
            $table->boolean('reference_required');
            $table->boolean('bills_included');
            $table->unsignedInteger('id_included');
            $table->foreign('id_included')->references('id_included')->on('includeds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenancy_details');
    }
}
