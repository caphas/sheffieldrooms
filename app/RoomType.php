<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    //
    protected $fillable = [
        'id_room_type', 'date_available', 'property_type' , 'room_type', 'available_couple'  
    ];
}
