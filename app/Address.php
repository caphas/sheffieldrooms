<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //

    protected $fillable = [
        'id_address', 'postcode', 'house_number','address_line_1', 'address_line_2', 'town'
    ];
}
