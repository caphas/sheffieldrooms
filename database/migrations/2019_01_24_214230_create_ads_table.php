<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id_ad');
            $table->string('advertiser_type',64)->nullable();
            $table->boolean('fee_applies')->nullable();
            $table->string('rent_type',26)->nullable();
            $table->string('proprety_type',64)->nullable();
            $table->string('youtube_url',255)->nullable();
            $table->string('email_contact',190)->nullable();
            $table->string('phone_contact',20)->nullable();
            $table->unsignedInteger('id_address');
            $table->unsignedInteger('id_room_type');
            $table->unsignedInteger('id_tenancy_detail');
            $table->unsignedInteger('id_existing_tenant');
            $table->unsignedInteger('id_preferance');
            $table->unsignedInteger('id_bedroom');
            $table->unsignedInteger('id');//id_user
            $table->foreign('id_address')->references('id_address')->on('addresses');
            $table->foreign('id_room_type')->references('id_room_type')->on('room_types');
            $table->foreign('id_tenancy_detail')->references('id_tenancy_detail')->on('tenancy_details');
            $table->foreign('id_existing_tenant')->references('id_existing_tenant')->on('existing_tenants');
            $table->foreign('id_preferance')->references('id_preferance')->on('preferances');
            $table->foreign('id_bedroom')->references('id_bedroom')->on('bedrooms');
            $table->foreign('id')->references('id')->on('users');//user
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
