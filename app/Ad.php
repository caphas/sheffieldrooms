<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    //
    public $timestamps = true;
    protected $fillable = [
        'id_ad', 'advertiser_type', 'fee_applies','rent_type', 'proprety_type', 'youtube_url','email_contact',
        'phone_contact', 'id_address','id_room_type', 'id_tenancy_detail', 'id_existing_tenant','id_preferance',
        'id_bedroom', 'id'//user
    ];
}
