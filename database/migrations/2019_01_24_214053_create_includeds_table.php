<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncludedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('includeds', function (Blueprint $table) {
            $table->increments('id_included');
            $table->boolean('council_tax');
            $table->boolean('water');
            $table->boolean('electricity');
            $table->boolean('gas');
            $table->boolean('wifi');
            $table->boolean('tv');
            $table->boolean('license');
            $table->boolean('cleaning');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('includeds');
    }
}
