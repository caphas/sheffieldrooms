@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <form>
                <div class="form-group">
                        <h1 style="color:#428bca;">1.Who are you ?</h1>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                        <label class="form-check-label" for="inlineRadio1">Live in landlord <span style="color:rgb(163, 163, 163);">(I own the property and live there)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Live out landlord <span style="color:rgb(163, 163, 163);">(I own the property but don't live there)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Current tenant/flatmate <span style="color:rgb(163, 163, 163);">(I am living in the property)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Agent <span style="color:rgb(163, 163, 163);">(I am advertising on a landlord's behalf)</span></label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Former flatmate <span style="color:rgb(163, 163, 163);">(I am moving out and need someone to replace me)</span></label>
                    </div>
                </div>
                <hr/>
                <div class="col-lg-8">
                    <div class="form-group">
                        <p> Fee applies ? (e.g. admin fees, tenant referencing, fees for drawing up tenancy agreements)</p>
                        <button type="button" class="btn btn-default">YES</button>
                        <button type="button" class="btn btn-primary">NO</button>
                        <p>Fees apply ?<br/> (e.g. admin fees, tenant referencing, fees for drawing up tenancy agreements)<br/>
                            Tick for Yes ( Landlords and Agents must disclose any non-optional fees when advertising rentals,
                            according to CAP rules. You should itemise what the fees cover, and how much they will cost each tenant in your description lower down. )</p>
                    </div>
                </div>
                <hr/>
                <h1 style="color:#428bca;">2. What are you renting ?</h1>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Room(s)</button>
                        <p>Advertise one or more rooms in a property e.g. house, flat.</p>    
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Entire Property/Studio</button>
                        <p>Advertise a whole property. This include houses, flats, including one-bedroom flats, studios and bedsits.
                        <br/>There are no existing flatmates and there will be a single contract.</p>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <h1 style="color:#428bca;">3. The address of the property to rent</h1>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter postcode here">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">FIND ADDRESS</button>
                            </span>
                        </div><!-- /input-group -->
                    </div>
                </div><!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="form-group">
                        <select class="custom-select">
                            <option selected>Please select -></option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h2 style="color:#428bca;">Or manually</h2>
                </div>            
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Postcode:</strong></label>
                        <input class="form-control" placeholder="Enter postcode here">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Flat or House Number (kept private):</strong></label>
                        <input class="form-control" placeholder="Enter postcode here">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Address line 1 (optional):</strong></label>
                        <input class="form-control" placeholder="eg. 10 Downing St or Westminster">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Address line 2 (optional):</strong></label>
                        <input class="form-control" placeholder="">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label><strong>Town:</strong></label>
                        <input class="form-control" placeholder="Town">
                    </div>
                </div>
                <hr/>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">4. Type of property</h1>
                </div> 
                <div class="col-lg-6">
                    <div class="form-group">
                        <select class="custom-select">
                            <option selected>Please select -></option>
                            <optgroup label="Room in a Shared Property">
                                <option value="1">Room in a Shared House</option>
                                <option value="2">Room in a Shared Flat</option>
                            </optgroup>
                            <optgroup label="Single Occupancy">
                                <option value="3">Studio</option>
                                <option value="4">Bedsit</option>
                            </optgroup>
                            <optgroup label="Entire House">
                                <option value="5">Detached House</option>
                                <option value="6">Semi-Detached House</option>
                                <option value="7">Terraced House</option>
                                <option value="8">End Terrance</option>
                                <option value="9">Bungalow</option>
                                <option value="10">Cottage</option>
                            </optgroup>
                            <optgroup label=" Entire Flat">
                                <option value="11">Flat</option>
                                <option value="12">Penthouse</option>
                                <option value="13">Maisonette</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">5. Room type</h1>
                </div> 
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label><strong>Date available * :</strong></label>
                        <input class="form-control" placeholder="eg. 10/10/19">
                    </div>
                    <div class="col-lg-6">
                        <label><strong>Property type :</strong></label>
                        <select class="custom-select">
                            <option selected>Please select -></option>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label><strong>Room type :</strong></label>
                        <select class="custom-select">
                            <option selected>Please select -></option>
                        </select>
                    </div>
                    <div class="col-lg-6">      
                        <label><strong>Available to couples :</strong></label>      
                        <div class="form-group row">
                            <label class="radio-inline col-lg-4">
                                <input type="radio" name="optionsRadiosInline" id="yesCheck" value="yes"><strong> Yes</strong>
                            </label>
                            <label class="radio-inline col-lg-4">
                                <input type="radio" name="optionsRadiosInline" id="noCheck" value="no"><strong> No</strong>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">6. Add photos/video</h1>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">File:</label>
                    <div class="col-lg-10">
                        <input id="input-b1" name="input-b1" type="file" class="file" data-browse-on-zone-click="true">
                    </div>
                </div> 
                <div class="form-group row">
                    <label><strong>Optional: Add YouTube share URL  :</strong></label>
                    <input class="form-control" placeholder="">
                </div>
                <div class="col-lg-12">
                    <h2 style="color:#428bca;">The Map: </h2>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">7. Provide information about the tenancy</h1>
                </div>
                <h3> Tenancy Details</h3>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>Monthly Rent For Entire Studio:</label>
                        <input class="form-control" placeholder="eg. 10/10/19">
                    </div>
                    <div class="col-lg-4">
                        <label>Minimum Tenancy Length</label>
                        <select class="custom-select">
                            <option selected>Please select -></option>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label>Earliest Move In Date:</label>
                        <select class="custom-select">
                            <option selected>Please select -></option>
                        </select>
                    </div>
                    <div class="col-lg-4">      
                        <label>Weekly Rent For Entire Studio:</label>      
                        <input class="form-control" placeholder="eg. 10/10/19">
                    </div>
                    <div class="col-lg-4">      
                        <label>Maximum Tenancy length (optional):</label>      
                        <input class="form-control" placeholder="eg. 10/10/19">
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">      
                        <label>Deposit Amount:</label>      
                        <input class="form-control" placeholder="eg. 10/10/19">
                    </div>
                    <div class="col-lg-4">      
                        <label>Maximum Number of Tenants:</label>      
                        <input class="form-control" placeholder="eg. 10/10/19">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Housing beenfit considered ?:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Reference required ?:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Bills:</strong></label>
                    <label class="col-lg-12">Tick appropriate box.</label>
                    <label class="col-lg-12"><strong> Bills included ?</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Some</button>
                    </div>
                </div>
                <div class="form-group">
                        <label><strong>Which are included :</strong></label>
                        <select class="selectpicker" multiple title="Choose one of the following...">
                            <option>Council Tax</option>
                            <option>Water</option>
                            <option>Electricity</option>
                            <option>Gas</option>
                            <option>Wi-Fi /BroadBand</option>
                            <option>TV</option>
                            <option>License</option>
                            <option>Cleaning</option>
                        </select>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">8. Bedroom</h1>
                </div>
                <div class="form-group">
                    <label><strong>Which are included :</strong></label>
                    <select class="selectpicker" multiple title="Choose one of the following...">
                        <option>The room has a Double Bed.</option>
                        <option>It is a double room (this means it's a big room, not that it has a double bed in it)</option>
                        <option>The room could go to a couple (you should mention rent adjustments in you description)</option>
                        <option>The Room has its own bathroom</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">9. Features about your property</h1>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">10. The existing tenants</h1>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">The existing household hates football
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">The existing household is vegetarian
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">The existing household are professionals
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">The existing household are students
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">The existing household has both professional and students
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">There is a cat
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="">There is a dog
                        </label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">Preference for new flatmate...</h1>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Gender:</strong></label>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No preference</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Female</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Male</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Mix</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Occupation:</strong></label>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No preference</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Professional</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Student</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Pets:</strong></label>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Age of tenant:</strong></label>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Couples welcome?:</strong></label>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Vegetarian preferred?:</strong></label>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">Yes</button>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-default">No</button>
                    </div>
                </div>
                <div class="col-lg-12">
                    <h1 style="color:#428bca;">How to contact you...</h1>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label class="col-lg-3">
                            <input type="checkbox" value="">Email: 
                        </label>
                        <input type="text" placeholder="example@mail.com">
                    </div>
                    <div class="checkbox">
                        <label class="col-lg-3">
                            <input type="checkbox" value="">Phone:
                        </label>
                        <input type="text" placeholder="Phone number">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8">
                        <label>By selecting Post My Ad you agree you've read and accepted our Terms of Use an Posting Rules. Please see our
                        Privacy Notice for information regarding the processing of your data</label>
                    </div>
                    <div class="col-lg-4">
                        <button type="submit" class="btn btn-danger">Post My ad</button>
                    </div>
                </div>
                <!--<div class="form-group row">
                    <label class="col-lg-12"><strong>Single Occupancy:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Studio</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Bedsit</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Entire House:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Semi-Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Terraced House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">End Terrance</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Bungalow</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Cottage</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-12"><strong>Entire Flat:</strong></label>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Semi-Detached House</button>
                    </div>
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-default">Terraced House</button>
                    </div>
                </div> -->
   

            </form>
        </div>
    </div>
</div>

<script>

</script>
@endsection
